#ifndef GAUSS_NEWTON_SOLVER_H
#define GAUSS_NEWTON_SOLVER_H

#include <vector>
#include <functional>

class GaussNewtonSolver
{
public:
    GaussNewtonSolver(int n, double step, double stop);

    void iterate();
    void setX(std::vector<double> X);
    std::vector<double> getX();
    void addFunction(std::function<double(std::vector<double> m_X)> f);
    void printX();
    void printErrors();
    bool checkStopCondition();

private:
    std::vector<std::function<double(std::vector<double> m_X)>> m_R;
    std::vector<double> m_X;
    double m_step;
    double m_stop;
};

#endif // GAUSS_NEWTON_SOLVER_H
