#include <iostream>
#include <vector>
#include <functional>
#include <fstream>
#include <string>

#include <Eigen/Dense>

#include "gauss_newton_solver.h"

void skipString(std::ifstream &input)
{
    std::string temp;
    input >> temp;
}

void loadMatrix(std::ifstream &input, Eigen::Matrix3d &matrix, int n, int m)
{
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < m; ++j) {
            double temp;
            input >> temp;
            matrix(i, j) = temp;
        }
    }
}

void loadVector(std::ifstream &input, Eigen::Vector3d &vector, int n)
{
    for (int i = 0; i < n; ++i) {
        double temp;
        input >> temp;
        vector(i) = temp;
    }
}

double sqr(double x)
{
    return x * x;
}

const double f = 500.0;
const int max_iterations = 500;

int main()
{
    std::ifstream input("input.txt");

    Eigen::Matrix3d R;
    Eigen::Vector3d T;
    int n;

    skipString(input);
    loadMatrix(input, R, 3, 3);
    skipString(input);
    loadVector(input, T, 3);
    skipString(input);
    input >> n;

    std::cout << "R = " << std::endl << R << std::endl << std::endl;
    std::cout << "T = " << std::endl << T << std::endl << std::endl;
    std::cout << "n = " << n << std::endl << std::endl;

    GaussNewtonSolver s(11, 0.00000001, 0.00005);

    for (int i = 0; i < n; ++i) {
        Eigen::Vector3d X_pcmm, X_camera;
        skipString(input);
        loadVector(input, X_pcmm, 3);
        skipString(input);
        loadVector(input, X_camera, 3);

        s.addFunction([X_pcmm, X_camera](std::vector<double> x){
            return sqr((x[8] * X_pcmm(0) + x[9] * X_pcmm(1) + x[10] * X_pcmm(2) + 1) * X_camera(0) -
                    (x[0] * X_pcmm(0) + x[1] * X_pcmm(1) + x[2] * X_pcmm(2) + x[3]));
        });

        s.addFunction([X_pcmm, X_camera](std::vector<double> x){
            return sqr((x[8] * X_pcmm(0) + x[9] * X_pcmm(1) + x[10] * X_pcmm(2) + 1) * X_camera(1) -
                    (x[4] * X_pcmm(0) + x[5] * X_pcmm(1) + x[6] * X_pcmm(2) + x[7]));
        });
    }

    s.addFunction([](std::vector<double> x){
        return x[0] * x[4] + x[1] * x[5] + x[2] * x[6];
    });

    s.addFunction([](std::vector<double> x){
        return x[0] * x[8] + x[1] * x[9] + x[2] * x[10];
    });

    s.addFunction([](std::vector<double> x){
        return x[4] * x[8] + x[5] * x[9] + x[6] * x[10];
    });

    s.addFunction([](std::vector<double> x){
        return (sqr(x[0]) + sqr(x[1]) + sqr(x[2])) - (sqr(x[4]) + sqr(x[5]) + sqr(x[6]));
    });

    s.addFunction([](std::vector<double> x){
        return sqr(f) * (sqr(x[8]) + sqr(x[9]) + sqr(x[10])) - (sqr(x[4]) + sqr(x[5]) + sqr(x[6]));
    });

    s.setX({10.1,
            0.2,
            4.1,
            10.4,
            0.1,
            4.1,
            0.6,
            0.1,
            4.2,
            80.1,
            0.05});

    int counter = 0;

    while (!s.checkStopCondition()) {
        s.iterate();
        ++counter;
        if (counter > max_iterations) {
            break;
        }
    }

    std::cout << "Errors = " << std::endl;
    s.printErrors();
    std::cout << std::endl;

    if (counter > max_iterations) {
        std::cout << "Solution not found!" << std::endl << std::endl;
    } else {
        std::cout << "Iterations = " << counter << std::endl << std::endl;
    }

    std::vector<double> x = s.getX();

    double t_z = 1 / std::sqrt(sqr(x[8]) + sqr(x[9]) + sqr(x[10]));

    Eigen::Matrix3d R_calculated;
    R_calculated << x[0] * t_z / f, x[1] * t_z / f, x[2] * t_z / f,
                    x[4] * t_z / f, x[5] * t_z / f, x[6] * t_z / f,
                    x[8] * t_z,     x[9] * t_z,     x[10] * t_z;

    Eigen::Vector3d T_calculated;
    T_calculated << x[3] * t_z / f, x[7] * t_z / f, t_z;

    std::cout << "R_calculated = " << std::endl << R_calculated << std::endl;
    std::cout << "T_calculated = " << std::endl << T_calculated;


    return 0;
}
