#include "gauss_newton_solver.h"

#include <armadillo>
#include <Eigen/Dense>

GaussNewtonSolver::GaussNewtonSolver(int n, double step, double stop) : m_step(step), m_stop(stop)
{
    m_X.reserve(n);
}

Eigen::MatrixXd inverse(const Eigen::MatrixXd &eigen_matrix)
{
    arma::mat arma_matrix(eigen_matrix.rows(), eigen_matrix.cols(), arma::fill::zeros);

    for (int i = 0; i < eigen_matrix.rows(); ++i) {
        for (int j = 0; j < eigen_matrix.cols(); ++j) {
            arma_matrix(i, j) = eigen_matrix(i, j);
        }
    }

    arma::mat arma_inv = inv(arma_matrix);
    Eigen::MatrixXd eigen_inv = Eigen::MatrixXd::Zero(arma_inv.n_rows, arma_inv.n_cols);

    for (int i = 0; i < arma_inv.n_rows; ++i) {
        for (int j = 0; j < arma_inv.n_cols; ++j) {
            eigen_inv(i, j) = arma_inv(i, j);
        }
    }

    return eigen_inv;
}

void GaussNewtonSolver::iterate()
{
    Eigen::MatrixXd J = Eigen::MatrixXd::Zero(m_R.size(), m_X.size());
    Eigen::VectorXd f_x = Eigen::VectorXd::Zero(m_R.size());

    for (int i = 0; i < m_R.size(); ++i) {
        auto f = m_R[i];
        f_x(i) = f(m_X);
        for (int j = 0; j < m_X.size(); ++j) {
            std::vector<double> tempX = m_X;
            tempX[j] += m_step;
            J(i, j) = (f(tempX) - f(m_X)) / m_step;
        }
    }

    Eigen::VectorXd x = Eigen::VectorXd::Zero(m_X.size());

    for (int j = 0; j < m_X.size(); ++j) {
        x(j) = m_X[j];
    }

    // x = x - (J.transpose() * J).inverse() * J.transpose() * f_x; // arma's matrix inverse works better
    x = x - inverse(J.transpose() * J) * J.transpose() * f_x;

    for (int j = 0; j < m_X.size(); ++j) {
        m_X[j] = x(j);
    }
}

void GaussNewtonSolver::setX(std::vector<double> X)
{
    m_X = X;
}

std::vector<double> GaussNewtonSolver::getX()
{
    return m_X;
}

void GaussNewtonSolver::addFunction(std::function<double (std::vector<double>)> f)
{
    m_R.push_back(f);
}

void GaussNewtonSolver::printX()
{
    for (int i = 0; i < m_X.size(); ++i) {
        std::cout << m_X[i] << std::endl;
    }
}

void GaussNewtonSolver::printErrors()
{
    for (int i = 0; i < m_R.size(); ++i) {
        std::cout << m_R[i](m_X) << std::endl;
    }
}

bool GaussNewtonSolver::checkStopCondition()
{
    for (int i = 0; i < m_R.size(); ++i) {
        if (std::fabs(m_R[i](m_X)) > m_stop) {
            return false;
        }
    }

    return true;
}
