#include <iostream>
#include <fstream>
#include <Eigen/Dense>
#include <vector>

const double f = 500.0;

const double delta_x = 0.1;
const double delta_y = 0.1;
const double delta_z = 0.1;

using namespace std;

int main()
{
    std::vector<Eigen::Vector3d> points;

    points.push_back(Eigen::Vector3d(0.0, 0.0, delta_z));

    points.push_back(Eigen::Vector3d(-delta_x, delta_y, 0.0));
    points.push_back(Eigen::Vector3d(0, delta_y, 0.0));
    points.push_back(Eigen::Vector3d(delta_x, delta_y, 0.0));

    points.push_back(Eigen::Vector3d(-delta_x, 2 * delta_y, 0.0));
    points.push_back(Eigen::Vector3d(0, 2 * delta_y, 0.0));
    points.push_back(Eigen::Vector3d(delta_x, 2 * delta_y, 0.0));

    points.push_back(Eigen::Vector3d(-delta_x, 3 * delta_y, 0.0));
    points.push_back(Eigen::Vector3d(0, 3 * delta_y, 0.0));
    points.push_back(Eigen::Vector3d(delta_x, 3 * delta_y, 0.0));

    Eigen::Matrix3d cameraMatrix;

    cameraMatrix << f,   0.0, 0.0,
                    0.0, f,   0.0,
                    0.0, 0.0, 1.0;

    double roll = 0.1;
    double pitch = 0.2;
    double yaw = -0.2;

    Eigen::Matrix3d Rx, Ry, Rz, R;

    Rx << 1.0, 0.0,       0.0,
          0.0, cos(roll), -sin(roll),
          0.0, sin(roll), cos(roll);

    Ry << cos(pitch),  0.0, sin(pitch),
          0.0,         1.0, 0.0,
          -sin(pitch), 0.0, cos(pitch);

    Rz << cos(yaw), -sin(yaw), 0.0,
          sin(yaw), cos(yaw),  0.0,
          0.0,      0.0,       1.0;

    R = Rx * Ry * Rz;

    Eigen::Vector3d T(0.3, 0.9, 1.1);

    if (!Eigen::Matrix3d::Identity().isApprox(R * R.transpose())) {
        std::cerr << "Non-orthogonal rotation matrix!" << std::endl;
        return 1;
    }

    std::ofstream outfile;

    outfile.open("input.txt");
    outfile.close();

    outfile.open("input.txt", std::ios::app);

    outfile << "R" << std::endl << R << std::endl << std::endl;
    outfile << "T" << std::endl << T << std::endl << std::endl;
    outfile << "n" << std::endl << points.size() << std::endl << std::endl;

    for (int i = 0; i < points.size(); ++i) {
        Eigen::Vector3d temp = (cameraMatrix * (R * points[i] + T));
        double s = temp(2);

        Eigen::Vector3d X_frame = temp / s;

        outfile << "pcmm_frame" << std::endl << points[i] << std::endl;
        outfile << "camera_frame" << std::endl << X_frame << std::endl << std::endl;
    }

    outfile.close();

    return 0;
}
